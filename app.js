/**
 * To run, you must have node and Phantomjs installed
 * then from this directory, call `phantomjs app.js`
 */

var page = require('webpage').create();

// set viewport sized
page.viewportSize = { width: 1024, height: 1500 };
// what is the image size
page.clipRect = { top: 0, left: 0, width: 1024, height: 1500 };

page.open('http://pressherald.com', function(){
   // evaluate the website. This is only needed 
   // since we have to change the background-color
   page.evaluate(function() {
      // make sure that the background-color is set
     document.body.bgColor = 'white';
     // wait three seconds just to make sure some stuff has loadsd
     setTimeout(function(){
         // render the image
         page.render('pph.png');
         // close out phantom
         phantom.exit();
      }, 3000);
   });
});